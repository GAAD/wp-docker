#! /bin/bash
echo 'APACHE helper'
#COPY server.crt /etc/apache2/ssl/server.crt
#COPY server.key /etc/apache2/ssl/server.key

cd /etc/apache2/ssl
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout server.key -out server.crt -subj '/CN=${DOMAIN}'

ls /etc/apache2/ssl

apache2 -D FOREGROUND