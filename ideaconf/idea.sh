#! /bin/bash
echo 'GAAD Idea configurator'


function checkLogFile( ){
    file=$1
    logdir=/var/.log/

    if [[ -f "${logdir}${file}" ]]; then echo 1; else echo 0; fi
}

function writeLogFile( ){
    file=$1
    text=$2
    logdir=/var/.log/

    echo ${text} > ${logdir}${file}
    chmod 777 ${logdir}${file}
    chgrp www-data ${logdir}${file}
    chown www-data ${logdir}${file}
}

function createLogDirectory() {
    if [[ ! -d "/var/.log" ]]; then
        mkdir /var/.log
        chmod 777 /var/.log
    fi

    chgrp www-data /var/.log
    chown www-data /var/.log
}

function addRunManagerComponent(){
     #-s '//project[@version="5"]' -t elem -n "component" \
    xmlstarlet edit --omit-decl --inplace \
      -s '//project[@version="4"]' -t elem -n "component" \
      -d '//project[@version="4"]/component[@name="RunManager"]' \
      -s '//project/component[last()]' -t attr -n "name" -v "RunManager" \
      -s '//project/component[last()]' -t elem -n "configuration" \
      -s '//project/component[last()]/configuration[last()]' -t attr -n "type" -v "PhpRemoteDebugRunConfigurationType" \
      -s '//project/component[last()]/configuration[last()]' -t attr -n "factoryName" -v "PHP Remote Debug" \
      -s '//project/component[last()]/configuration[last()]' -t attr -n "filter_connections" -v "FILTER" \
      -s '//project/component[last()]/configuration[last()]' -t attr -n "name" -v "${DOMAIN}" \
      -s '//project/component[last()]/configuration[last()]' -t attr -n "server_name" -v "${DOMAIN}" \
      -s '//project/component[last()]/configuration[last()]' -t elem -n "method" \
      -s '//project/component[last()]/configuration[last()]/method[last()]' -t attr -n "v" -v "2" \
      workspace.xml
}

function addPhpServersComponent(){
     #-s '//project[@version="5"]' -t elem -n "component" \
    xmlstarlet edit --omit-decl --inplace \
      -s '//project[@version="4"]' -t elem -n "component" \
      -d '//project[@version="4"]/component[@name="PhpServers"]' \
      -s '//project/component[last()]' -t attr -n "name" -v "PhpServers" \
      -s '//project/component[last()]' -t elem -n "servers" \
      -s '//project/component[last()]/servers[last()]' -t elem -n "server" \
      -s '//project/component[last()]/servers[last()]/server[last()]' -t attr -n "host" -v "${DOMAIN}" \
      -s '//project/component[last()]/servers[last()]/server[last()]' -t attr -n "id" -v "eb0c3089-a19d-473e-afa4-e092c2d0fb80" \
      -s '//project/component[last()]/servers[last()]/server[last()]' -t attr -n "name" -v "${DOMAIN}" \
      -s '//project/component[last()]/servers[last()]/server[last()]' -t attr -n "use_path_mappings" -v "true" \
      -s '//project/component[last()]/servers[last()]/server[last()]' -t elem -n "path_mappings" \
      -s '//project/component[last()]/servers[last()]/server[last()]/path_mappings[last()]' -t elem -n "mapping" \
      -s '//project/component[last()]/servers[last()]/server[last()]/path_mappings[last()]/mapping[last()]' -t attr -n "local-root" -v "\$PROJECT_DIR$" \
      -s '//project/component[last()]/servers[last()]/server[last()]/path_mappings[last()]/mapping[last()]' -t attr -n "remote-root" -v "/var/www/html" \
      -s '//project/component[last()]/servers[last()]/server[last()]/path_mappings[last()]' -t elem -n "mapping" \
      -s '//project/component[last()]/servers[last()]/server[last()]/path_mappings[last()]/mapping[last()]' -t attr -n "local-root" -v "\$PROJECT_DIR$/index.php" \
      -s '//project/component[last()]/servers[last()]/server[last()]/path_mappings[last()]/mapping[last()]' -t attr -n "remote-root" -v "/var/www/html/index.php" \
      workspace.xml
}
createLogDirectory

if [[ `checkLogFile .idea-init` = 0 ]]
  then
    addRunManagerComponent
    addPhpServersComponent
    writeLogFile .idea-init "Project .idea directory updated"

    else
        echo "Projects .idea directory is up to date"
fi