#!/bin/bash -e
COMPOSE_PROJECT_NAME=$(grep COMPOSE_PROJECT_NAME .env | xargs)
COMPOSE_PROJECT_NAME=${COMPOSE_PROJECT_NAME#*=}

HOST_FILE=$(grep HOST_FILE .env | xargs)
HOST_FILE=${HOST_FILE#*=}

PROJECT_DOMAIN=$(grep PROJECT_DOMAIN .env | xargs)
PROJECT_DOMAIN=${PROJECT_DOMAIN#*=}


#
# Function https://github.com/mkropat/sh-realpath
# (for poor Mac users)
_canonicalize_dir_path() {
    (cd "$1" 2>/dev/null && pwd -P)
}
_canonicalize_file_path() {
    local dir file
    dir=$(dirname -- "$1")
    file=$(basename -- "$1")
    (cd "$dir" 2>/dev/null && printf '%s/%s\n' "$(pwd -P)" "$file")
}


__DIR__="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
DOCKER_COMPOSE_DIR=$(_canonicalize_dir_path ${__DIR__}/..)

# Window and mac user should adapt the next line
HOST_FILE=/etc/hosts

cd $DOCKER_COMPOSE_DIR

WEB_IP=$( docker inspect  --format='{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' ${COMPOSE_PROJECT_NAME}_apache )

PHP_CONTAINER_NAME=`docker-compose images php | grep --color=never php | cut -f 1 -d \ `


# Set the /etc/hosts
. .env

sudo sed -i'' -e "/# web_container_IP - ${COMPOSE_PROJECT_NAME}/d" ${HOST_FILE}
sudo sed -i'' -e "/^.*${PROJECT_DOMAIN}.*\$/d" ${HOST_FILE}
printf "\n# web_container_IP - ${COMPOSE_PROJECT_NAME}\n" | sudo tee -a  ${HOST_FILE} > /dev/null
printf "${WEB_IP}\t${PROJECT_DOMAIN}\n" | sudo tee -a ${HOST_FILE} > /dev/null

