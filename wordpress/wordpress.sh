#! /bin/bash

echo 'Wordpress install'

function checkWPInstall() {
  if [[ -d "${PROJECT_WORKING_DIR}/wp-admin" ]]; then echo 1; else echo 0; fi
}

function checkProjectInitialDataFile() {
  if [[ -f "${PROJECT_WORKING_DIR}/${PROJECT_DB_DATA_PATH}" ]]; then echo 1; else echo 0; fi
}

function coreDownload() {
  echo 'Wordpress core download'
  wp core download --path=/tmp/wp/ --version=${WP_VERSION} --locale=${WP_LOCALE} --force
}

function coreConfig() {
  if [[ ! -f "/tmp/wp/wp-config.php" ]]; then
    echo 'Setting up wp-config.php file'
    wp core config --path=/tmp/wp/ --dbname=${MYSQL_DATABASE} --dbuser=${MYSQL_USER} --dbpass=${MYSQL_PASSWORD} --dbhost=${MYSQL_CONTAINER} --dbprefix=${WP_TABLE_PREFIX} --locale=${WP_LOCALE}

    sed -i "s/define( 'DB_COLLATE', '' );/define( 'DB_COLLATE', '' );\ndefine( 'FS_METHOD', 'direct' );/g" /tmp/wp/wp-config.php

  else
    echo "File wp-config.php already exists"
  fi
}

function coreInstall() {

  if [[ $(checkWPInstall) == 0 ]]; then
    echo 'Installing Wordpress core'
    wp core install --path=/tmp/wp/ --url=${PROJECT_DOMAIN} --title=${PROJECT_DOMAIN} --admin_user=${WP_ADMIN_USER} --admin_password=${WP_ADMIN_PASS} --admin_email=${WP_ADMIN_EMAIL}
  else
    echo "Wordpress installed already"
  fi
}

function waitForDB() {
  INTERVAL=5
  #check if db container is ready
  while ! mysql -h "${MYSQL_CONTAINER}" -u "${MYSQL_USER}" -p"${MYSQL_PASSWORD}" "${MYSQL_DATABASE}" -e 'SHOW GLOBAL STATUS LIKE "Uptime"' --connect_timeout=2; do
    sleep ${INTERVAL}
    echo "Waiting ${INTERVAL} sec. and test will again"
  done
  echo 'DB connection ready'
}

function waitForDBInit() {
  INTERVAL=5
  echo "Database initialization test"
  while true; do

    MYSQL_CHECKDATA=$(mysql -h ${MYSQL_CONTAINER} -u ${MYSQL_USER} -p${MYSQL_PASSWORD} ${MYSQL_DATABASE} --skip-column-names -e "SHOW TABLES FROM ${MYSQL_DATABASE} LIKE '${WP_TABLE_PREFIX}users';")

    if [ "${MYSQL_CHECKDATA}" = "${WP_TABLE_PREFIX}users" ]; then
      echo "Database initialized"
      break
    else
      echo "Waiting ${INTERVAL} sec. and test will again"
    fi
    sleep ${INTERVAL}
  done
}

function syncInstallationFiles() {
  if [[ $(checkWPInstall) == 0 ]]; then
    rsync --ignore-existing -r /tmp/wp/ ${PROJECT_WORKING_DIR}/
  fi
}

function installGitignore() {
  if [[ ! -f "${PROJECT_WORKING_DIR}/.gitignore" ]]; then
    # shellcheck disable=SC2164
    cd /tmp/
    echo "*" >/tmp/.gitignore &&
      echo "!wp-content/" >>/tmp/.gitignore &&
      echo "!wp-content/*" >>/tmp/.gitignore
    cat .gitignore >"${PROJECT_WORKING_DIR}"/.gitignore
  fi
}

function wordpressCleanUp() {
  rm -fr /tmp/wp/wp-content/languages/*
  rm -fr /tmp/wp/wp-content/themes/*
  rm -fr /tmp/wp/wp-content/plugins/*
}

function installProjectInitialData() {
  if [[ $(checkProjectInitialDataFile) == 1 ]]; then
    echo "Installing database from ${PROJECT_WORKING_DIR}/${PROJECT_DB_DATA_PATH}"
    mysql -h ${MYSQL_CONTAINER} -u ${MYSQL_USER} -p${MYSQL_PASSWORD} ${MYSQL_DATABASE} <"${PROJECT_WORKING_DIR}/${PROJECT_DB_DATA_PATH}"
    coreConfig
  else
    echo "Unable to find database initialization file in ${PROJECT_WORKING_DIR}/${PROJECT_DB_DATA_PATH}"
  fi
}

function dockerUserId() {
  mysql -sNh ${MYSQL_CONTAINER} -u ${MYSQL_USER} -p${MYSQL_PASSWORD} ${MYSQL_DATABASE} -e "SELECT count(id) FROM ${WP_TABLE_PREFIX}users WHERE user_login = \"${WP_ADMIN_USER}\";"
}

function getCurrentDomainFromDb() {
  CURRENT_DOMAIN_IN_DB=$(wp option get siteurl --path=/var/www/html/ | tail -n 1)
  CURRENT_DOMAIN_IN_DB=$(echo ${CURRENT_DOMAIN_IN_DB} | awk -F[/:] '{print $4}')
  echo "${CURRENT_DOMAIN_IN_DB}"
}

function projectNeedsDomainReplace() {
  CURRENT_DOMAIN_IN_DB=$(getOutputLastLine getCurrentDomainFromDb)
  if [[ "${CURRENT_DOMAIN_IN_DB}" != "${PROJECT_DOMAIN}" ]]; then
    echo 1
    else
  echo 0
  fi
}

function isDomainReplaceAllowed() {
  if [[ "${PROJECT_DOMAIN_REPLACE}" == "allow" ]]; then
    echo 1
  else
    echo 0
  fi
}

function isUploadsFolderFromExternalRepositoryInstalled() {
  if [[ -d "$(pwd)/project-assets/uploads" ]]; then
    echo 1
  else
    echo 0
  fi
}

function replaceDbDomainToEnvsDomain() {
  echo "Domain replace from ${CURRENT_DOMAIN_IN_DB} to ${PROJECT_DOMAIN}"
  wp search-replace "$(getCurrentDomainFromDb)" "${PROJECT_DOMAIN}" --path=/var/www/html/
}

function domainReplaceManager() {
  if [[ "$(isDomainReplaceAllowed)" == 1 ]]; then
    if [[ "$(projectNeedsDomainReplace)" == 1 ]]; then
      replaceDbDomainToEnvsDomain
    else
      echo "Domain replace not needed"
    fi
  else
    echo "Domain replace is not allowed"
  fi
}

function dockerUserCreate() {
  USER_EXISTS=$(dockerUserId)
  if [[ "${USER_EXISTS}" == '0' ]]; then
    wp user create ${WP_ADMIN_USER} wp@${PROJECT_DOMAIN} --path=/var/www/html --role=administrator --user_pass=${WP_ADMIN_PASS}
    echo "Wordpress user created ${WP_ADMIN_USER}/${WP_ADMIN_PASS}"
  else
    echo "Wordpress user ${WP_ADMIN_USER} already exists"
  fi
}

function getOutputLastLine() {
  COMMAND=$1
  $(${COMMAND} > /tmp/getOutputLastLine.tmp 2>&1)
  cat /tmp/getOutputLastLine.tmp | tail -n 1
}

function handleProjectAssetsLink() {
  if [[ "$(isUploadsFolderFromExternalRepositoryInstalled)" == 1 ]]; then
    rm -fr $(pwd)/wp-content/uploads
    ln -s ../project-assets/uploads wp-content/uploads
  fi
}

#check if Wordpress installation is necessary
if [[ $(checkWPInstall) == 0 ]]; then
  echo 'Wordpress not found, installing...'
  coreDownload
  wordpressCleanUp
  waitForDB
  if [[ $(checkProjectInitialDataFile) == 1 ]]; then
    installProjectInitialData
  else
    coreConfig
    coreInstall
  fi

  waitForDBInit
  syncInstallationFiles

  domainReplaceManager
  dockerUserCreate
  installGitignore
  handleProjectAssetsLink
fi

echo "This is a idle script (infinite loop) to keep container running."

cleanup() {
  kill -s SIGTERM $!
  exit 0
}

trap cleanup SIGINT SIGTERM

while [ 1 ]; do
  sleep 60 &
  wait $!
done
